<?php
/*
*
* Add Credits
* Hook version 1.1.0
*
**/
if (!defined("WHMCS"))
    die("This file cannot be accessed directly");

/*********************
Auto Add Credits Settings
 *********************/
function addCredits_settings()
{
    return array(
        'apiuser'		=> '', // one of the admins username
    );
}
/********************/

function addCredits_accept($vars)
{
    $settings = addCredits_settings();
    $ispaid = true;
    $userid = 0;

    if($vars['invoiceid'])
    {
        $result = localAPI('getinvoice', array(
            'invoiceid' 		=> $vars['invoiceid'],
        ), $settings['apiuser']);

        $ispaid = ($result['result'] == 'success') ? true : false;
        $userid = ($result['result'] == 'success') ? $result['userid'] : 0;
    }

    if($ispaid)
    {
	    //check if item description contains apiCredits and add credits automatically
        if(strpos($result['items']['item'][0]['description'], 'apiCredits') !== false) {
            $addCredits = localAPI('AddCredit', array(
                'clientid' => $userid,
                'description' => 'Adding funds via api',
                'amount' => $result['total'],
                'adminid' => '', // whmcs admin id
            ), $settings['apiuser']);
        }

    }
}

add_hook('InvoicePaid', 1, 'addCredits_accept');

?>

