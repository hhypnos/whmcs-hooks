<?php
/*
*
* Auto Accept Orders And Delete Pending Orders
* Hook version 1.5.2
*
**/
if (!defined("WHMCS"))
    die("This file cannot be accessed directly");

/*********************
Auto Accept Orders Settings
 *********************/
function autoAcceptOrders_settings()
{
    return array(
        'apiuser'		=> '', // one of the admins username
        'autosetup' 		=> false, // determines whether product provisioning is performed
        'sendregistrar' 	=> false, // determines whether domain automation is performed
        'sendemail' 		=> false, // sets if welcome emails for products and registration confirmation emails for domains should be sent
        'ispaid'		=> true, // set to true if you want to accept only paid orders
        'paymentmethod'		=> array(), // set the payment method you want to accept automaticly (leave empty to use all payment methods) * example array('paypal','amazonsimplepay')
    );
}
/********************/

function autoAcceptOrders_accept($vars)
{
    $settings = autoAcceptOrders_settings();

    $ispaid = true;
    $userid = 0;

    if($vars['InvoiceID'])
    {
        $result = localAPI('getinvoice', array(
            'invoiceid' 		=> $vars['InvoiceID'],
        ), $settings['apiuser']);

        $ispaid = ($result['result'] == 'success' && $result['balance'] <= 0) ? true : false;
        $userid = ($result['result'] == 'success') ? $result['userid'] : 0;
    }

    if((!sizeof($settings['paymentmethod']) || sizeof($settings['paymentmethod']) && in_array($vars['PaymentMethod'], $settings['paymentmethod'])) && (!$settings['ispaid'] || $settings['ispaid'] && $ispaid))
    {
        $result = localAPI('AcceptOrder', array(
            'orderid' 		=> $vars['orderId'],
            'autosetup' 		=> $settings['autosetup'],
            'sendregistrar' 	=> $settings['sendregistrar'],
            'sendemail' 		=> $settings['sendemail'],
        ), $settings['apiuser']);

        // Fetch Pending Orders By userID
        $result2 = localAPI('GetOrders', array(
            'userid' 		=> $userid,
            'status' 		=> 'Pending',
            'limitnum' 	=> 9999999,
        ), $settings['apiuser']);

        // Find Unpaid Orders and delete
        if($result2['result']=="success"){
            for($i=0;$i<$result2['totalresults'];$i++) {
                $cancelOrder = localAPI('CancelOrder', array(
                    'orderid' 		=> $result2['orders']['order'][$i]['id'],
                    'cancelsub' => 1,
                ), $settings['apiuser']);

                $deleteOrder = localAPI('DeleteOrder', array(
                    'orderid' 		=> $result2['orders']['order'][$i]['id'],
                ), $settings['apiuser']);
            }
        }
    }
}

add_hook('OrderPaid', 1, 'autoAcceptOrders_accept');

?>

